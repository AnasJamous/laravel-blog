<?php

namespace App\Http\Controllers;

use App\Models\Post;

class BlogController extends Controller
{
    public function getIndex()
    {
        $posts = Post::paginate(4);

        return view('blog.index')->with('posts', $posts);
    }

    public function getSingle($slug)
    {
        // Fetch from the DB based on slug
        $post = Post::where('slug', '=', $slug)->first();

        return view('blog.single')->with('post', $post);
    }
}
