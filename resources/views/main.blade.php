<!doctype html>
<html lang="en">
    <head>
        @include('inc._head')
    </head>
    <body>
        @include('inc._nav')
        <div class="container mt-4"  style="margin-top: 6em !important;">
            @include('inc._messages')
            @yield('content')
            @include('inc._footer')
        </div> <!-- end of .container -->
            @include('inc._javaScript')
            @yield('scripts')
    </body>
</html>
