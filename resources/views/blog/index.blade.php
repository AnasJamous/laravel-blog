@extends('main')

@section('title', '| Blog')

@section('content')
    <div class="row mb-4">
      <div class="col-md-8 offset-md-2 mb-4 bg-info text-light text-spacing">
        <h1 class="text-center"> Blog </h1>
      </div>
    </div>

    @foreach ($posts as $post)
        
      <div class="row mb-4">
        <div class="col-md-8 offset-md-2">
          <h2> {{$post->title}} </h2>
          <h5>
            Published: {{ date('M j, Y', strtotime($post->created_at)) }}
          </h5>
          <p> {{substr($post->body, 0, 250)}} {{strlen($post->body) > 250 ? '...' : ''}} </p>
          
          <a
           href="{{ route('blog.single', $post->slug) }}"
           class="btn btn-info"
          >
            Read More
          </a>
          <hr>
        </div>
      </div>
    @endforeach

    <div class="d-flex">
      <div class="mx-auto item-center">
          {{$posts->links("pagination::bootstrap-4")}}
      </div>
    </div>
@endsection