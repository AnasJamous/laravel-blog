@extends('main')

@section('title', '| HomePage')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1 class="display-4">Welcome To My Blog</h1>
                <hr class="my-4">
                <p><a class="btn btn-primary btn-lg" href="#" role="button">Pppular Posts</a></p>
            </div>
        </div>
    </div> <!-- end of .row -->

    <div class="row">
        <div class="col-md-8">
            @foreach ($posts as $post)
                <div class="post">
                    <h3> {{$post->title}} </h3>
                    <p> {{substr($post->body, 0, 200)}} {{strlen($post->body) > 200 ? '...' : ''}} </p>
                    <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">Read More</a>
                </div>
                <hr>
            @endforeach
        </div>

        <div class="col-md-3 offset-md-1">
            <h2>sidebar</h2>
        </div>

    </div>
</div> <!-- end of .container -->

@endsection

{{--@section('scripts')--}}
{{--    <script >--}}
{{--        // confirm('I loaded')--}}
{{--    </script>--}}
{{--@endsection--}}
