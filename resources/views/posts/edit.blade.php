@extends('main')

@section('title', '| Edit Blog Post')

{{-- @section('stylesheets')
    {!! Html::style('css/parsley.css') !!}
@endsection --}}

@section('content')
{!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) !!}
<div class="row">
	<div class="col-md-8">
		{{ Form::label('title', 'Title:') }}
		{{ Form::text('title', null, ['class' => 'form-control']) }}

		{{ Form::label('slug', 'Slug:', ['class' => 'form-spacing-top']) }}
		{{ Form::text('slug', null, ['class' => 'form-control']) }}

		{{ Form::label('body', 'Body:', ['class' => 'form-spacing-top']) }}
		{{ Form::textarea('body', null, ['class' => 'form-control']) }}
	</div>

	<div class="col-md-4">
		<div class="well customize-well">
			<dl class="dl-horizontal">
				<dt>Created At: </dt>
				<dd> {{ date('M j, Y h:i a', strtotime($post->created_at)) }} </dd>
			</dl>

			<dl class="dl-horizontal">
				<dt>Last Updated:</dt>
				<dd> {{ date('M j, Y h:i a', strtotime($post->updated_at)) }} </dd>
			</dl>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					{!! Html::linkRoute('posts.show', 'Cancel', [$post->id], ['class' => 'btn btn-danger btn-block']) !!}
					{{-- <a href="" class="btn btn-primary btn-block">Edit</a> --}}
				</div>
				<div class="col-sm-6">
					{{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
					{{-- {!! Html::linkRoute('posts.update', 'Save Changes', [$post->id], ['class' => 'btn btn-success btn-block']) !!} --}}
					{{-- <a href="#" class="btn btn-danger btn-block">Delete</a> --}}
				</div>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}
@endsection

{{-- @section('scripts')
    {!! Html::script('js/parsley.min.js') !!}
@endsection --}}
