@extends('main')

@section('title', '| View Post')

@section('content')

    <div class="row">
        <div class="col-md-8">
            <h1> {{ $post->title }} </h1>
            <p class="lead"> {{$post->body}} </p>
        </div>

        <div class="col-md-4 customize-well">
            <div class="well">
                <dl class="dl-horizontal">
                    <dt>Url:</dt>
                    <p> 
                        <a href="{{ url('blog/'.$post->slug) }}">
                            {{ url('blog/'.$post->slug) }}
                        </a>
                    </p>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Created At:</dt>
                    <p> {{ date('M j, Y h:i a', strtotime($post->created_at)) }} </p>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Last Updated:</dt>
                    <p> {{ date('M j, Y h:i a', strtotime($post->updated_at)) }} </p>
                </dl>
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        {!! Html::linkRoute('posts.edit', 'Edit', [$post->id], ['class' => 'btn btn-primary btn-block']) !!}
                        {{-- <a href="" class="btn btn-primary btn-block">Edit</a> --}}
                    </div>
                    <div class="col-sm-6">
                        {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) !!}
                        
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ Html::linkRoute('posts.index', '<< See All Posts', [], ['class' => 'btn btn-block btn-spacing']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
